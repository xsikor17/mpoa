/*
 * xsikor_LCD.h
 *
 *  Created on: Dec 28, 2019
 *      Author: Roman
 */

#ifndef INC_XSIKOR_LCD_H_
#define INC_XSIKOR_LCD_H_

#include "stm32f4xx_hal.h"

#define NUM_KEY_ROW 4
#define NUM_KEY_COL 3

typedef struct struktura{
    uint16_t xpos_relative;
    uint16_t ypos_relative;
    uint16_t xsize;
    uint16_t ysize;
    uint8_t label[5];
}TKEY;


uint8_t xsikor_LCD_INIT(void);
void xsikor_TS_INIT(void);
void xsikor_make_keyboard(TKEY key[NUM_KEY_ROW][NUM_KEY_COL], uint16_t kb_xsize, uint16_t kb_ysize);
void xsikor_print_keyboard(uint16_t xpos, uint16_t ypos, TKEY key[NUM_KEY_ROW][NUM_KEY_COL]);
void xsikor_key_index(int16_t *index_row, int16_t *index_column, uint16_t TS_xpos, uint16_t TS_ypos,TKEY keyboard[NUM_KEY_ROW][NUM_KEY_COL]);
void xsikor_vypis_casy(int32_t cas[3]);
#endif /* INC_XSIKOR_LCD_H_ */
