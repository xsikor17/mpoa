/*
 * xsikor_LCD.c
 *
 *  Created on: Dec 28, 2019
 *      Author: Roman
 */

#include "xsikor_LCD.h"
#include "stm32f429i_discovery_lcd.h"
#include "stm32f429i_discovery_ts.h"
#include <string.h>

uint8_t xsikor_LCD_INIT(void){
    /*BSP LCD INICIALIZACE (preinicializuje MX_LTDC_INIT, ktery je nefunkcni v CUBE)*/
    uint8_t ret;

    ret = BSP_LCD_Init();
    BSP_LCD_LayerDefaultInit(1, LCD_FRAME_BUFFER);
    BSP_LCD_SelectLayer(1);

    /* Clear the LCD */

    BSP_LCD_DisplayOn();
    BSP_LCD_Clear(LCD_COLOR_RED);
    return ret;
}

void xsikor_TS_INIT()
{
    uint8_t status;

    status = BSP_TS_Init(BSP_LCD_GetXSize(), BSP_LCD_GetYSize());

    if (status != TS_OK)
    {
      BSP_LCD_Clear(LCD_COLOR_RED);
      BSP_LCD_SetBackColor(LCD_COLOR_RED);
      BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
      BSP_LCD_DisplayStringAt(0, LINE(5), (uint8_t *)"TOUCHSCREEN", CENTER_MODE);
      BSP_LCD_DisplayStringAt(0, LINE(6), (uint8_t *)"INIT FAIL, TRY RECONECT THE TARGET", CENTER_MODE);
      while(1);
    }
    else
    {
      BSP_LCD_Clear(LCD_COLOR_GREEN);
      BSP_LCD_SetBackColor(LCD_COLOR_GREEN);
      BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
      BSP_LCD_DisplayStringAt(0, LINE(5), (uint8_t *)"TOUCHSCREEN", CENTER_MODE);
      BSP_LCD_DisplayStringAt(0, LINE(6), (uint8_t *)"INIT OK", CENTER_MODE);
      HAL_Delay(1000);
      BSP_LCD_Clear(LCD_COLOR_WHITE);
    }

    return;
}

void xsikor_make_keyboard(TKEY key[NUM_KEY_ROW][NUM_KEY_COL], uint16_t kb_xsize, uint16_t kb_ysize)
{
    uint8_t lab='1';

    for(uint8_t i=0; i<NUM_KEY_ROW;i++)
    {
        for(uint8_t y=0; y<NUM_KEY_COL;y++)
        {
            key[i][y].xsize = kb_xsize/NUM_KEY_COL;
            key[i][y].ysize = kb_ysize/NUM_KEY_ROW;
            key[i][y].xpos_relative = y*key[i][y].xsize;
            key[i][y].ypos_relative = i*key[i][y].ysize;
            key[i][y].label[0] = lab ;
            key[i][y].label[1] = '\0' ;

            if(lab == '1'+ 9)
            {
                key[i][y].label[0] = 'G';
                key[i][y].label[1] = 'E';
                key[i][y].label[2] = 'N';
                key[i][y].label[3] = '\0';
            }
            if(lab == '1' + 10)
            {
                key[i][y].label[0] = '0';
            }
            if(lab == '1' + 11)
            {
                key[i][y].label[0] = 'C';
                key[i][y].label[1] = 'L';
                key[i][y].label[2] = 'R';
                key[i][y].label[3] = '\0';
            }
            lab++;
        }
    }
    return;
}

void xsikor_print_keyboard(uint16_t xpos, uint16_t ypos, TKEY key[NUM_KEY_ROW][NUM_KEY_COL])
{

   HAL_Delay(17); //zpozdeni nutne kvuli rychlosti obnovovani displeje
      for(uint8_t y=0;y<NUM_KEY_COL;y++)
      {
           for (uint8_t i=0;i<NUM_KEY_ROW;i++)
           {
                BSP_LCD_SetTextColor(LCD_COLOR_GRAY);
                BSP_LCD_FillRect(xpos+key[i][y].xpos_relative, ypos+key[i][y].ypos_relative, key[i][y].xsize, key[i][y].ysize);
                BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
                BSP_LCD_DrawRect(xpos+key[i][y].xpos_relative, ypos+key[i][y].ypos_relative, key[i][y].xsize, key[i][y].ysize);
                BSP_LCD_SetFont(&Font20);
                BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
                BSP_LCD_SetBackColor(LCD_COLOR_GRAY);
                BSP_LCD_DisplayStringAt((xpos+key[i][y].xpos_relative+(key[i][y].xsize/2))-(Font20.Width/2),(ypos+key[i][y].ypos_relative+(key[i][y].ysize/2))-(Font20.Height/2),key[i][y].label, LEFT_MODE );

                if (!strcmp(key[i][y].label, "GEN"))
                {
                    BSP_LCD_SetTextColor(LCD_COLOR_GREEN);
                    BSP_LCD_FillRect(xpos+key[i][y].xpos_relative, ypos+key[i][y].ypos_relative, key[i][y].xsize, key[i][y].ysize);
                    BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
                    BSP_LCD_DrawRect(xpos+key[i][y].xpos_relative, ypos+key[i][y].ypos_relative, key[i][y].xsize, key[i][y].ysize);
                    BSP_LCD_SetFont(&Font20);
                    BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
                    BSP_LCD_SetBackColor(LCD_COLOR_GREEN);
                    BSP_LCD_DisplayStringAt((xpos+key[i][y].xpos_relative+(key[i][y].xsize/2))-(3*Font20.Width/2),(ypos+key[i][y].ypos_relative+(key[i][y].ysize/2))-(Font20.Height/2),key[i][y].label, LEFT_MODE );
                }

                if (!strcmp(key[i][y].label, "CLR"))
                {
                    BSP_LCD_SetTextColor(LCD_COLOR_RED);
                    BSP_LCD_FillRect(xpos+key[i][y].xpos_relative, ypos+key[i][y].ypos_relative, key[i][y].xsize, key[i][y].ysize);
                    BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
                    BSP_LCD_DrawRect(xpos+key[i][y].xpos_relative, ypos+key[i][y].ypos_relative, key[i][y].xsize, key[i][y].ysize);
                    BSP_LCD_SetFont(&Font20);
                    BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
                    BSP_LCD_SetBackColor(LCD_COLOR_RED);
                    BSP_LCD_DisplayStringAt((xpos+key[i][y].xpos_relative+(key[i][y].xsize/2))-(3*Font20.Width/2),(ypos+key[i][y].ypos_relative+(key[i][y].ysize/2))-(Font20.Height/2),key[i][y].label, LEFT_MODE );
                }
           }

      }
      BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
      return;
}

/* funkce xsikor_key_index();
 * funkce pro urceni indexu zmacknute klavesy.
 * V pripade ze klavesa existuje tak na adrese index_row a index_column jsou ulozeny jeji indexy
 * Pokud klavesa neexistuje, na techto adresach je ulozeno -1
 */
void xsikor_key_index(int16_t *index_row, int16_t *index_column,uint16_t TS_xpos, uint16_t TS_ypos, TKEY keyboard[NUM_KEY_ROW][NUM_KEY_COL])
{
    for(uint8_t y=0;y<NUM_KEY_COL;y++)
     {
          for (uint8_t i=0;i<NUM_KEY_ROW;i++)
          {
              if((TS_xpos > keyboard[i][y].xpos_relative) && (TS_xpos < (keyboard[i][y].xpos_relative + keyboard[i][y].xsize)))
              {
                  if((TS_ypos > keyboard[i][y].ypos_relative) && (TS_ypos < (keyboard[i][y].ypos_relative + keyboard[i][y].ysize)))
                  {
                      *index_row = i;
                      *index_column = y;
                      return;
                  }
              }
          }
     }
    *index_row = -1;
    *index_column = -1;
    return;
}

void xsikor_vypis_casy(int32_t cas[3])
{
	uint8_t vypis[20];
	BSP_LCD_SetFont(&Font12);
	BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
	BSP_LCD_SetBackColor(LCD_COLOR_WHITE);

	BSP_LCD_DisplayStringAtLine(10, "Doby vypoctu 10k vzorku v [ms]");
	sprintf(vypis,"CORDIC FLOAT - %d", cas[0]);
	BSP_LCD_DisplayStringAtLine(11,vypis );
	sprintf(vypis,"MATH.h - %d", cas[1]);
	BSP_LCD_DisplayStringAtLine(12,vypis);
	sprintf(vypis,"ARM_MATH.h - %d", cas[2]);
	BSP_LCD_DisplayStringAtLine(13,vypis);
	BSP_LCD_SetFont(&Font20);
	BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
	BSP_LCD_SetBackColor(LCD_COLOR_GRAY);
}
