function [xnew ynew] = cordic_rotacni_faz_inkrement_do_360(x,y,fazovy_inkrement,Niter,Ki,Korekce_Uhlu)
%funkce umi vytvorit sinus a cosinus s jakymkoliv 2*pi*f*t ale do 360

%blok osetreni, pokud bude fazovy inkrement vets� nez 90 stupnu, odecte se
%od nej nasobek 90 stupnu tak aby se dostal do rozsahu 0-90 stupnu, podle
%toho kolikrat se 90 stupnu odecte musime vypoctene vzorky upravit viz
%konec teto funkce
if  (fazovy_inkrement <= pi/2)
    pomocny_fazovy_inkrement = fazovy_inkrement;    
elseif (fazovy_inkrement > pi/2 && fazovy_inkrement<=pi)
    pomocny_fazovy_inkrement = fazovy_inkrement-(pi/2);
elseif (fazovy_inkrement > pi && fazovy_inkrement<=(3*pi/2))
    pomocny_fazovy_inkrement = fazovy_inkrement-(pi);
elseif (fazovy_inkrement > (3*pi/2) && fazovy_inkrement<= (2*pi))
    pomocny_fazovy_inkrement = fazovy_inkrement-(3*pi/2);
end


[xnew ynew] =cordic_rotacni_prvni_kvadrant(x,y,pomocny_fazovy_inkrement,Niter,Ki,Korekce_Uhlu); 

  
if (fazovy_inkrement > pi/2 && fazovy_inkrement<=pi)
    pom=xnew;
    xnew=-ynew;
    ynew=pom;
elseif (fazovy_inkrement > pi && fazovy_inkrement <=(3*pi/2))
    xnew=-xnew;
    ynew=-ynew;
elseif (fazovy_inkrement > (3*pi/2) && fazovy_inkrement <= (2*pi))
    pom=-xnew;
    xnew=ynew;
    ynew=pom;
end


end

