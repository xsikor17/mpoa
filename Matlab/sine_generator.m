clear all
close all

%vstupni parametry
f=100; %frekvence generovaneho signalu
fvz=8000; %vzorkovaci frekvence
tvz=1/fvz;
pocet_period = 15; %pocet zobrazovanych period
fazovy_inkrement = 2*pi*f*tvz; %uhel natoceni mezi vzorky

Niter=10; %pocet iteraci
Ki=zeros(1,Niter);
Korekce_Uhlu=zeros(1,Niter);
for i=0:Niter
    Ki(i+1)=cos(atan(2^(-i)));
    Korekce_Uhlu(i+1) = atan(2^-i);
end

T=0:tvz:pocet_period*(1/f);
cosine = zeros (1,length(T));
sine = zeros (1,length(T));
matlab_cosine = zeros (1,length(T));
matlab_sine = zeros (1,length(T));

cosine(1)=1;
sine(1)=0;
matlab_cosine(1)=1;
matlab_sine(1)=0;


for i=2:length(T)
  [cosine(i) sine(i)] = cordic_rotacni_faz_inkrement_do_360(cosine(i-1),sine(i-1),fazovy_inkrement,Niter,Ki,Korekce_Uhlu);  
end



for i=2:length(T)
   matlab_cosine(i) = cos((i-1)*fazovy_inkrement);
   matlab_sine(i) = sin((i-1)*fazovy_inkrement);
end


stem (T,cosine)
hold on
stem (T,matlab_cosine)