%%algoritmus ktery otoci vstupni vektor o pozadovane dphi (v radianech),
%%tato hodnota muze nabyvat maximalne 99.8 stupne protoze hodnota
%%atan(2^-i) ktera pocita uhel konverguje k teto hodnote a nikdy ji
%%neprekroci
function [ xnew,ynew ] = cordic_base( x,y,dphi,Niter )
figure
newangle=unwrap(angle(x+j*y)+dphi);
line([0 x],[0 y])
lvys=line([0 abs(x+j*y)*cos(newangle)], [0 abs(x+j*y)*sin(newangle)]);
set(lvys,'color','green');
viscircles([0,0], abs(x+j*y));
axis([-1.3*abs(x+j*y) 1.3*abs(x+j*y) -1.3*abs(x+j*y) 1.3*abs(x+j*y)]);

%%pocatecni podminky od kterych zaciname rotovat
xtemp=x;
ytemp=y;
z=dphi;

for i=0:Niter
Ki=cos(atan(2^(-i))); %vypocet konstanty pro korekci vektoru

disp(strcat('Puvodni uhel=',num2str(360*angle(x+j*y)/(2*pi))));
disp(strcat('aktualni uhel=',num2str(360*angle(xtemp+j*ytemp)/(2*pi))));
disp(strcat('Pozadovany rozdil uhlu=',num2str(360*(dphi)/(2*pi))));
disp(strcat('Rozdil uhlu=',num2str(360*unwrap(-angle(x+j*y)+angle(xtemp+j*ytemp))/(2*pi))));
 pause(0.1)

xpom=xtemp;
ypom=ytemp;

xtemp=Ki*(xpom-sign(z)*ypom*2^(-i)); %%hodnota d je zde urcena jako sign(z) protoze v 'z' se kumuluje hodnota o kterou se musime otocit
ytemp=Ki*(ypom+sign(z)*xpom*2^(-i)); %%a vzdy je bud kladna nebo zaporna
z=z-sign(z)*atan(2^-i);

hold on
plot(xtemp, ytemp,'or')
lo=line([0 xtemp],[0 ytemp]);
set(lo,'color','red');

end
xnew = xtemp;
ynew = ytemp;
end

