function [ xnew ynew ] = cordic_rotacni_prvni_kvadrant( x,y,dphi,Niter,Ki,Korekcni_Uhel )
%UNTITLED Summary of this function goes here

%   Detailed explanation goes here
xtemp=x;
ytemp=y;
z=dphi;

for i=0:Niter

xpom=xtemp;
ypom=ytemp;

if z<0
    znamenko = -1;
else
    znamenko = 1;
end

xtemp=Ki(i+1)*(xpom-znamenko*ypom*2^(-i)); %%hodnota d je zde urcena jako sign(z) protoze v 'z' se kumuluje hodnota o kterou se musime otocit
ytemp=Ki(i+1)*(ypom+znamenko*xpom*2^(-i)); %%a vzdy je bud kladna nebo zaporna
z=z-znamenko*Korekcni_Uhel(i+1);%atan(2^-i);

end
xnew = xtemp;
ynew = ytemp; 


end

