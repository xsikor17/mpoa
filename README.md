# Zadání
Realizujte generátor harmonického signálu na vývojové desce STM32F4DISCOVERY s využitím algoritmu „COordinate ROtation DIgital Computer“. 
Výstupem generátoru bude sluchátkový JACK. Výpočet vzorků signálu realizujte v pohyblivé řádové čárce a v pevné řádové čárce. Srovnejte dobu výpočtu vzorku a porovnejte s výpočtem pomocí knihovny math.h.
Ovládání generátoru realizujte pomocí displeje na vývojové desce STM32F429. Na displeji rovněž vhodně zobrazte srovnání doby výpočtů vzorků.

# Teoretický rozbor
## Algoritmus CORDIC (COordinate ROtation DIgital Computer):
Jedná se o matematický iterační algoritmus sloužící zejména k přepočtu mezi kartézskými a polárními souřadnicemi.
To lze vodně vužít i pro generaci harmonických signálů, jak bude jasné na konci teoretického rozboru. Podstatou algoritmu je zjednodušit výpočet goniometrických funkcí sin() a cos() tak,
 aby nebyl výpočet neoptimální pro případnou implementaci do mikrokontroleru, či DSP procesoru.
 
Algoritmus vychází z následujících goniometrických vztahů:


![Goniometricke vztahy](obrazky/goniom_vztahy_vychozi.PNG)

Na začátku výpočtu se vychází z vektoru:

![Vychozi vektor](obrazky/vychozi_vektory.PNG)

Pokud vektor budeme chtít rotovat, tedy v polárních souřadnicích měnit jeho uhel, bude výsledek následující:

![Rotace vektoru](obrazky/rotace_vektory.PNG)

Po aplikaci goniometrických vztahů:

![Cordic 1](obrazky/cordic1_vztah.PNG)

Drobná úprava vztahu:

![cordic 2](obrazky/cordic2_vztah.PNG)

Základní myšlenkou CORDICu je:

*		Výběr úhlu θ tak, aby tan⁡(θ) nabýval hodnota 2^(-i), pro celočíselné kladné i.
*		Rotace téměř o libovolný úhel leze rozdělit na θ=θ1+θ2+θ3+...

Po aplikaci těchto předpokladů:

![cordic 3](obrazky/cordic3_vztah.PNG)

Je tedy k dispozici soustava rovnic, která umožňuje vypočítat rotaci (hodnoty v kartézské soustavě souřadnic) o zadaný úhel v polárních souřadnicích.

Hodnota **di** určuje zda se v dané iteraci uhel **θi** přičítá či odečítá.

Výpočet vzorku obsahuje základní aritmetické operace jako sčítání, odečítání a násobení. Dále je zde bitový posun. 

Je jasné, že každý vzorek se bude počítat v počtu iterací rovnému „i“, a výsledná přesnost bude na tomto počtu závislá.

Korekční člen Ki bude pro každý výpočet stejný, a je možné jeho hodnoty uložit do paměti. To se provede například po zapnutí mikrokontroleru. 
Doba výpočtu vzorků už poté není tímto výpočtem ovlivněná. Je možné dokázat, že lze tuto korekci v jednotlivých iteracích vynechat a až výsledný 
vzorek vynásobit hodnotou **0.6073**. K této hodnotě lze dojít vynásobením všech **Ki** mezi sebou. Matematicky lze toto násobení získat, pokud bychom **Ki** 
z jednotlivých iterací vytkli. Nicméně pro pevnou řádovou čárku toto není možné, protože mezivýsledky iterací by překročily rozsah použitého formátu **Q0.15**.

Úhel rotace se pro generaci signálu určí jako:

![fazovy inkrement](obrazky/fazovy_inkrement.PNG)

Problém je, že součet dílčích úhlů **arctg(2^-i))**, vzhledem k průběhu funkce tangent, je něco málo přes 99°. Pokud je rotace úhlu větší, je třeba algoritmus upravit.

Pro maximální přesnost by byl vhodný datový typ s plovoucí řádovou čárkou. Na obrázku se nachází přibližná přesnost výpočtu závislá na počtu iterací v prostředí Matlab.

![chyba vypoctu](obrazky/chyba_rad_float.png)

Pro 12 iterací je chyba menší než 10^(-5).

Názorný výpočet vzorků, a generace signálu je možné vyzkoušet v přiložených souborech „implementace_cordic_base.m“ a „sine_generator.m“. Druhý soubor generuje cos() pomocí
CORDICU i funkce cos() a zapisuje je do grafu. Je možné vyzkoušet vliv počtu iterací a tedy i přesnosti výpočtu na výsledný signál.

Pokud přesnost výpočtu klesne, je možné si všimnout, že vzdálenost mezi vzorky je dobrá, nicméně se kumuluje chyba, a po větším množství vzorků signál změní fázi oproti počátečnímu stavu. 
To by byl problém v systémech, kde je nutné obnovit fázi nosné. Naopak výhodou CORDICU je, že počítá vzorky sin i cos zároveň. To muže být s výhodou využito v kvadraturních modulacích.


Závěrem je třeba zdůraznit, že výhodou CORDICU je velmi malá náročnost na paměť oproti například generaci z tabulky hodnot. Dále je výhodou možnost generování teoreticky jakéhokoliv 
kmitočtu omezeného pouze přesností použitého formátu čísel. Nevýhodou pak je zmíněné ztracení informace o počáteční fázi vzhledem ke kumulaci chyby mezi vzorky. Je tedy nutné, 
pokud je tato informace podstatná, provádět korekci výpočtu.

Opravit tedy každý x-ty vzorek například výpočtem pomocí klasické funkce sin() a cos(), jak je to i naznačeno v tomto projektu. 
Pokud se vzorky předpočítají do nějaké fronty, pak má procesor dostatek času na výpočet korekční hodnoty klasickou metodou.

## Implementace algoritmu
 
Algoritmus je implementován vývojovém kitu STM32F4DISCOVERY. Tento procesor má jednotku pro počty v pohyblivé řádové čárce, takže je algoritmus implementován pro datový typ *float*.
Výpočet v pevné řádové čárce využívá dvou formátu a to Q0.15 a Q3.28. Pro počítání vzorku je využit první formát z toho důvodu, že procesor nemá intrinsické funkce pro násobení dvou 32 bitových znaménkových hodnot 
a bylo by tak nutné čísla přesto oříznout a výsledek by opět obsahoval kvantovací chybu. 
Formát Q3.28 je použit pro výpočet fázového inkrementu, a následné určování znaménka **di** algoritmu (viz. Vzorec.). Vzhledem k tomu, že fázový inkrement je v radiánech může být velmi malý a při použité velké
vzorkovací frekvence je nutná velká přesnost čísla. Intrinsickými funkcemi pro sčítání a odečítání 32 bitových hodnot se saturací procesor disponuje. Jsou jimi funkce *_QADD()* a *_QSUB*.

### Zdroje pro teoretický rozbor:
1.	Maršálek Roman – CORDIC, prezentace předmětu MIKS. Dostupná ve složce projektu
2.	Doplňující informace https://en.wikipedia.org/wiki/CORDIC


# Ralizace

Součástí práce jsou dva projekty. Jeden projekt realizuje algoritmus CORDIC na vývojové desce *STM32F4DISCOVERY*. Druhý se zabývá realizací ovládání generátoru na displeji vývojové desky *STM32F429I*.

## STM32F4DISCOVERY

Projekt obsahuje dvě hlavní dvojice souborů *cordic.c (.h)* a *cordic_fixed.c (.h)*. Ty obsahují funkce pro kompletní realizaci algoritmu CORDIC. Soubor *main.c* zajišťuje inicializaci periferií, realizaci komunikace UART,
 předávání vypočtených vzorků D/A převodníku a měření doby výpočtu 10k  vzorků různými způsoby.

### Cordic.c (.h)

Soubor obsahující výpočet vzorku ve formátu *float*. Obsahuje následující funkce:

```C
void vypocet_korekcni_uhel(void);
```
Funkce, která spočítá hodnoty dílčích úhlů **θi** a uloží je do pole. 

```C
void vypocet_korekce_amplitudy(void);
```

Funkce, která spočítá hodnoty korekčních koeficientů Ki pro zadaný  počet iterací a uloží je do pole.

```C
void vypocet_mocniny(void);
```

Vzhledem k absenci bitového posunu ve formátu double obsahuje tato funkce výpočet mocnin 2^(-i) pro zadaný počet iterací, které uloží do pole.

```C
void vypocet_TDPHI(TDPHI* dphi, double fazovy_inkrement);
```
Funkce vypočítá redukovaný fázový inkrement, a určí, do kterého kvadrantu by nový vzorek zasahoval (po rotaci vektoru [1 0]). Jedná se o zmíněnou úpravu algoritmu v případě,
že je rotace úhlu více jak 90°. Tehdy je možné odečíst od fázového inkrementu násobky 90°. To způsobí, že se výpočet provede v prvním kvadrantu (do 90°). Díky faktu, že se rotuje vzorky po jednotkové kružnici
je poté možné pouze změnit znaménka vypočtených souřadnic a obdržet tak správný výsledek. To zajišťuje následující funkce, která změní znaménka souřadnic nového vzorku, aby odpovídal rotaci o neredukovaný fázový inkrement.
 
```C
 
void cordic_rotacni(float *x, float *y, TDPHI* dphi)
{
	static float xnew,ynew;

	xnew = *x;
	ynew = *y;

	cordic_rotacni_PK(&xnew,&ynew,dphi->redukovany_fazovy_inkrement);

    if (dphi->kvadrant == 1){
	    *x = xnew;
	    *y = ynew;
	}else if (dphi->kvadrant == 2){
	    *x = -ynew;
	    *y = xnew;
	}else if (dphi->kvadrant == 3){
		*x = -xnew;
		*y = -ynew;
	}else if (dphi->kvadrant == 4){
		*x = ynew;
		*y = -xnew;
	}
	return;
}

```

Pro přehlednější práci je pro uložení redukovaného inkrementu a čísla kvadrantu vytvořena struktura TDPHI. Aby nedocházelo ke zbytečnému kopírování hodnot, 
v případě, že by funkce nebyla void ale vracela by hodnotu TDPHI, je vstupem adresa proměnné typu TDPHI a funkce pracuje přímo s ní. 
Parametr fazovy	inkrement muže nabývat hodnot <0 , 2pi> 

Hlavní funkce souboru. Vypočítá rotaci vzorku pro jakýkoliv fázový_inkrement v rozsahu 0 – (pi/2) stupně. 
Aby nedocházelo ke zbytečnému kopírování hodnot a kvůli faktu, že funkce aktualizuje vždy pouze 2 hodnoty, jsou funkci předány souřadnice starého vzorku v podobě adresy.
Tělo funkce obsahuje následující syntaxi.

```C
 
void cordic_rotacni_PK(float *x, float *y, float fazovy_inkrement)
{
	static float xtemp,ytemp,z,xold,yold;
	static uint16_t i;


	xtemp=*x;
	ytemp=*y;
	z=fazovy_inkrement;

	for (i=0 ; i<Niter ; i++)
	{
		xold = xtemp;
		yold = ytemp;

		if (z < 0)
		{
			xtemp = ((KorekceAmplitudy[i]*(xold)) + (KorekceAmplitudy[i]*yold * mocnina[i]));
			ytemp = ((KorekceAmplitudy[i]*(yold)) - (KorekceAmplitudy[i]*xold * mocnina[i]));
			z = z + KorekcniUhel[i];
		}else{
			xtemp = ((KorekceAmplitudy[i]*(xold)) -(KorekceAmplitudy[i]*yold * mocnina[i]));
			ytemp = ((KorekceAmplitudy[i]*(yold)) +(KorekceAmplitudy[i]*xold * mocnina[i]));
			z = z - KorekcniUhel[i];
		}

	}
    *x = xtemp;
	*y = ytemp;

	return;

}

```

Na začátku si funkce překopíruje vstupní parametry do interních proměnných. *Xtemp* a *ytemp* jsou proměnné obsahující nové hodnoty po výpočtu jedné iterace. 
Proměnná *z* obsahuje číslo, o jak velký úhel je třeba ještě vzorek pootočit.
Smyčka, která proběhne zadaný počet iterací, vytvoří na začátku „starý“ vzorek. Následuje podmínka *if*, která zjišťuje, zda je zbytkový úhel *z* záporný či ne, a nahrazuje znaménko **di** ve vzorci (viz. teoretický úvod).
V závislosti na tomto znaménku (tedy na hodnotě *z*) je vypočten vzorek příslušnými rovnicemi. Na konci funkce se změní hodnota na adrese původních vzorků vstupujících do funkce.


**Cordic.h**

Tento soubor obsahuje definování struktury TDPHI. Dále obsahuje nastavení počtu iterací, se kterými bude prováděn výpočet a deklaraci funkcí.
Součástí souboru je rovněž makro realizující převod čísla v pohyblivé řádové čárce na formát **Q0.15**.

```C
#define	FLOAT2FIXED(x)	((x) >= 0 ? ((int16_t)((x)*32768+0.5)) : ((int16_t)((x)*32768-0.5)))
```

### Cordic_fixed.c (.h)

Tento soubor obsahuje upravené funkce předchozích souborů, nyní pro pevnou řádovou čárku. V této kapitole jsou popsány hlavní rozdíly.
Jak bylo zmíněno, jsou ve výpočtech použity dva formáty. Pro přepočet z plovoucí řádové čárky jsou opět definovány makra:

```C
#define	FLOAT2FIXED(x)	((x) >= 0 ? ((int16_t)((x)*32768+0.5)) : ((int16_t)((x)*32768-0.5)))
#define	FLOAT2FIXED3_28(x)	((x) >= 0 ? ((int32_t)((x)*268435456+0.5)) : ((int32_t)((x)*268435456-0.5)))
```

Vysvětlení jak se makro tvoří je pod definicí makra v cordic_fixed.h.

Funkce

```C
void vypocet_korekcni_uhel_fixed(void);
```

a 

```C
void vypocet_korekce_amplitudy_fixed(void);
```

jsou realizovány obdobně jak v případě float, jsou pouze doplněny o převod na pevnou řádovou čárku.
Funkce pro výpočet amplitudových korekčních členů je navíc upravena tak, že do pole ukládá záporné hodnoty.
Důvodem je to, že jejich hodnota konverguje k 1. Nicméně Q0.15 neumí vyjádřit 1 ale pouze 0,99996…
To by reálně mohlo způsobit lineární tlumení signálu, když každý další vzorek by byl nepatrně přitlumen. Po pár periodách by byl signál ztracen.
Vlivem záporných hodnot je nutné, aby byl počet iterací pro výpočet sudý.

Výpočet mocniny 2^(-i) a její úkládání do pole je nahrazen bitovým posuvem přímo ve výpočtu iterací vzorku. 

```C
void vypocet_TDPHI_fixed(TDPHI_FIXED* dphi_fixed, Int32 fazovy_inkrement_fixed);
```

Tato funkce se liší od té v plovoucí řádové čárce pouze tím, že pracuje v pevné řádové čárce a využívá intrinsické 
funkce *_QSUB()* pro odečtení hodnot se satruací.

```C
void cordic_rotacni_PK_fixed(Int16 *x, Int16 *y, Int32 fazovy_inkrement_fixed);
```

Veškeré funkce násobení, sčítání a odečítání jsou nahrazeny intrinsickými funkcemi. Pří zápisu bylo nutné dbát na správné bitové posuvy jednotlivých čísel, 
aby nedošlo ke ztrátě dat. Část 

```C
(int32_t)__SMUAD(yold,KorekceAmplitudy_fixed[i]))>>posun
```

přímo nahrazuje výpočet Ki * Y * 2^(-i). Zde je zakomponovaný bitový posun. Z předešlých zkušeností může vzniknout v jazyce C problém, že pokud by počet iterací překročil hodnotu 64 
(ikdyž reálně více iterací jak cca 16 vzhledem k použité přesnosti a formátu nemá cenu) začne se do čísla nasouvat nedefinované znaky. Ošetření je v tomto případě realizováno tak, že 
při počtu iterací vyšším než 34 se bitový posun zasaturuje právě na tuto hodnotu. 

```C
void cordic_rotacni_fixed(Int16 *x, Int16 *y, TDPHI_FIXED* dphi_fixed);
```

Tato funkce obsahuje oproti float verze jednak změnu znaménka za použití intrinsické funkce a zároveň obsahuje možný způsob korekce (viz. teoretický rozbor) každého KOREKCE_FIXED vzorku (definováno v .h souboru).

```C
#if 1		//aktivace korekce
			if(((i & (KOREKCE_FIXED-1)) == 0))
			{
				korekce_x_fp = cos((((KOREKCE_FIXED))*fazovy_inkrement)+fazova_korekce_x_fp);
				korekce_y_fp = sin((((KOREKCE_FIXED))*fazovy_inkrement)+fazova_korekce_x_fp);


				fazova_korekce_x_fp = acos(korekce_x_fp);

				if(korekce_y_fp<0)
					fazova_korekce_x_fp = DVA_PI - fazova_korekce_x_fp;

				if (korekce_x_fp >0.9999694824)
					korekce_x_fp = 0.9999694824;
				if (korekce_y_fp > 0.9999694824)
					korekce_y_fp = 0.9999694824;
				lastx_fp=FLOAT2FIXED(korekce_x_fp);
				lasty_fp=FLOAT2FIXED(korekce_y_fp);
			}
#endif
```

Je zde použita forma kruhové paměti, kdy KOREKCE_FIXED je mocnina 2. Hodnota *i* je neznaménkové 32bitové číslo, ve kterém sledujeme jen určitou část bitů. 
Pokud se rovnají 0, a je nutná korekce, vypočítá se hodnota sin() a cos() pro *i* násobek *fázového inkrementu*. Hodnota fazova_korekce_x_fp je uhel v rozsahu 0-360 stupňů 
a je to v podstatě hodnota *i* násobku *fázového inkrementu* redukovaná na nejmenší možný úhel. Tento úhel se dá považovat za jakýsi offset. Vzhledem k tomu, že se *i* začne čítat znovu od nuly, je nutné tento 
offset v další korekci (za KOREKCE_FIXED vzorků) připočítat.
Fázová korekce se ještě musí upravit podle toho, zda je žádaná hodnota acos() 0-90 či 270-360 stupňů. To se samozřejmě určí podle hodnoty funkce sinus.

### Problém s realizací

Při realizaci projektu se bohužel nepodařilo docílit funkční implementace algoritmu výpočtu vzorků v pevné řádové čárce. Ačkoliv jsou knihovny s touto implementací přiloženy k projektu, jsou zde pouze pro naznačení, jak by 
se realizace v *Q* formátu dalo docílit. 

Jsou zde dva možné důvody nefunkčnosti. Prvním, méně pravděpodobným, důvodem je, že intrinsické funkce použité ve výpočtech kolidují s použitými formáty čísel. Ani přes pokusy přetypovat parametry nedošlo ke změně funkčnosti. 
Další, pravděpodobnější, důvod je ten, že bitové posuny, použité ve výpočtech, mají problém s formátem *Q* uloženým v klasickém datovém typu *int32_t* a nasouvají do čísel nesprávné hodnoty.

Z těchto důvodů je pro výpočet vzorků pro D/A převodník použitý algoritmus implementovaný v plovoucí řádové čárce. Jeho rychlost ovšem omezuje použitelnou vzorkovací frekvenci na 48 kHz, ačkoliv se dá předpokládat, že výpočet v pevné řádové
čárce by byl rychlejší a umožnil by vzorkovat 96 kHz. Ovšem vzhledem k tomu, že je generován zvuk pro sluchátkový výstup, je zadávání frekvence omezeno na 20 kHz.  Pro tuto frekvenci je i při vzorkovacím kmitočtu 48 kHz splněn vzorkovací teorém. 

### main.c

Kostra projektu je vygenerovaná v prostředí STM32CubeIDE. Všechny periferie jsou inicializovány do výchozího stavu. Navíc je povolena periferie *UART2*. Dále jsou k projektu nakopírovány hlavičkové soubory pro práci s D/A převodníkem
prostřednictvím funkcí *BSP* knihovny.

Na začátku kódu je připojeno několik nutných hlavičkových souborů. Jsou to právě zmíněné soubory algoritmu CORDIC, dále je zde soubor pro práci s audio výstupem, zároveň je připojena knihovna *math.h* a *arm_math.h* pro porovnání
rychlostí výpočtu.

V programu jsou definovány konstanty pro použitou vzorkovací frekvenci a velikost kruhové paměti pro ukládání vzorků výstupního signálu (velikost musí být mocnina 2). Výhodou kruhové paměti je zejména to, že není nutné ošetřovat indexaci v případě dosažení konce bloku paměti.
Dále je v kódu deklarováno několik globálních proměnných. Jedná se o deklaraci kruhové paměti, indexů pro čtení a zápis, a nutné základní proměnné pro práci s algoritmem CORDIC. Kruhová paměť je definovaná jako *static* aby nebylo možné k ní přistupovat
z jiných zdrojových souborů kódu.

```C
/* USER CODE BEGIN PD */
#define CIRC_BUFFER_SIZE (1<<8)
#define SAMPL_FREQ (48000)
/* USER CODE END PD */

static int16_t circ_buf[CIRC_BUFFER_SIZE];
uint32_t read_index=0,write_index=CIRC_BUFFER_SIZE/2;

/*for fixed calc*/
TDPHI_FIXED dphi_fp;
int16_t lastx_fp=FLOAT2FIXED(0.99),lasty_fp=0;
int32_t fazovy_inkrement_fp;
/*for fixed calc*/


/*for float CORDIC*/
TDPHI dphi;
float lastx=1,lasty=0;
float fazovy_inkrement;
/*for float CORDIC*/
/* USER CODE END PV */
```

Dále je deklarovaná funkce, která určuje dobu nutnou pro výpočet *X* vzorků. V programu je to 10 000 vzorků.

```C
void mereni_casu(uint32_t pocet_vzorku);
```

Funkce obsahuje 3 smyčky (pro algoritmus CORDIC v pohyblivé řádové čárce, dále pro výpočet pomocí knihovny math.h a nakonec pro výpočet pomocí optimalizované knihovny pro ARM, tedy arm_math.h), a v každé provede výpočet vzorků funkce *sinus* a *cosinus* (algoritmus CORDIC počítá obě tyto funkce najednou), a převod vzorku do pevné řádaové čárky.
Čas výpočtu je získáván pomocí funkce *HAL_GetTick()*, která vrací počet milisekundových "ticku" od začátku programu.  Na konci funkce se naměřená data odešle po rozhraní UART v pevně daném formátu.

Hlavní funkce programu inicializuje periferie a ještě před nekonečnou smyčkou jsou volány funkce pro výpočet tabulek hodnot nuných ke správné funkci CORDICu.

```C
 vypocet_korekcni_uhel();
 vypocet_mocniny();
 vypocet_korekce_amplitudy();
 vypocet_TDPHI(&dphi,fazovy_inkrement);
```

Nekonečná smyčka obsahuje jednu smyčku *while*, která zajišťuje aby nedocházelo k přepsání vzorků v kruhové paměti. Pokud je paměť plná, program zůstane v této smyčce a až dojde k vyčtení vzorku, program tuto smyčku opustí a vypočítá další vzorek.
Pokud je program ve zmíněné smyčce tak kontroluje, zda nebyla přijata nová frekvence z řídícího panelu. Příjem je realizován prostřednictvím přerušení. Je tedy použita neblokující funkce *HAL_UART_Receive_IT()*. Až se přijme po rozhraní UART znak **#**, který signalizuje
poslední vyslaný znak z řídícího panelu, dojde ke zpracování přijatých dat. Zpracováním je myšlen převod do číselného formátu, spuštění či zastavení generace zvuku, změna stavu signalizační LED diody a přepočet fázového inkrementu. 

```C
while(((write_index + 1) & (CIRC_BUFFER_SIZE - 1)) == read_index)
     {
    	HAL_UART_Receive_IT(&huart2, data, 6);
        if(data[5] == '#'){

  		  sscanf(data, "%d#", &lcd_frekv);
  		  memset(data,0,16);
  		  if (lcd_frekv == 0)
  		  {
  			  BSP_LED_Off(LED3);
  			  BSP_AUDIO_OUT_Pause();
  		  }else
  		  {
  			  BSP_LED_On(LED3);
  			  BSP_AUDIO_OUT_Resume();
  		  }
  		  /*Tyto parametry je nutne prepocitat pri zmene frekvence*/
  		  fazovy_inkrement = 2 * JEDNA_PI * lcd_frekv / SAMPL_FREQ;
  		  fazovy_inkrement_fp = FLOAT2FIXED3_28(fazovy_inkrement);
  		  vypocet_TDPHI(&dphi,fazovy_inkrement);
  		  vypocet_TDPHI_fixed(&dphi_fp,fazovy_inkrement_fp);
  		  /**/
        }

     };
```

Mimo tuto smyčku dochází k výpočtu vzorků a jejich zápisu do kruhové paměti. Nutné je ošetření maximální hodnoty, kdy formát Q0.15 neumožňuje zápis hodnoty 1, ale pouze 0,99996... Takže je vzorek, v případě hodnoty 1 saturován na maximální hodnotu formátu Q0.15.

```C
 cordic_rotacni(&lastx,&lasty,&dphi); //vypocet vzorku
/*format Q0.15 nemá hodnotu 1, proto nutna saturace, jinak makro FLOAT2FIXED vratí chybnou hodnotu*/
if (lastx>0.9999694824)
	lastx=0.9999694824;
if (lasty>0.9999694824)
 lasty=0.9999694824;
/**/
circ_buf[write_index]=FLOAT2FIXED(lastx);  //vkladani do kruhove pameti
write_index = (write_index + 1) & (CIRC_BUFFER_SIZE - 1); //inkrementace zapisovaciho indexu včetne maskovani podle delky pameti
```
Až jsou pomocí DMA řadiče přehrány dané vzorky, je volaná callback funkce *BSP_AUDIO_OUT_TransferComplete_CallBack()*. Tento callback zajišťuje odeslání dalších vzorků z kruhové paměti do paměti obsluhované řadičem DMA pomocí funkce BSP_AUDIO_OUT_ChangeBuffer().


```C
void BSP_AUDIO_OUT_TransferComplete_CallBack(void)
{
	static uint16_t send_buf[2];
    if( read_index == write_index)
        BSP_AUDIO_OUT_ChangeBuffer( 0, 1);	//pokud v pameti nejsou vzorky k dispozici vklada se nula
    else {
    	send_buf[0]=circ_buf[read_index]; //ulozeni vzorku pro 1. kanal
    	send_buf[1]=circ_buf[read_index]; //ulozeni tehoz vzorku pro 2. kanal
    	/*V pripade ze by se odesilal pouze jeden vzorek, tak jeden ze stereo kanalu nefunguje spravne, a frekvence generovaneho signalu druheho kanalu je presne 2 krat vetsi nez pozadovana.*/
        BSP_AUDIO_OUT_ChangeBuffer(send_buf, 2);
        read_index = (read_index + 1) & (CIRC_BUFFER_SIZE -1); //inkrementace indexu pro cteni v kruhove pameti
    }
}
```

## STM32F429I DISCOVERY

Na tomto vývojovém kitu je realizován ovládací panel generátoru. Ovládání displeje a jeho dotykové části je realizováno pomocí *BSP* funkcí. Projekt, opět generovaný v STM32CubeIDE, je nutné rozšířit o potřebné knihovny. Do adresáře projektu
bylo nutné nakopírovat složky BSP, Components a Utilities z existujícího funkčního projektu starší verze prostředí STM32Cube jelikož soubory nové verze vykazovaly chybu. Dále bylo nutné v jednotlivých hlavičkových souborech upravit cesty (v #include) k ostatním potřebnýn hlavičkovým souborům.

Hlavní funkce kódu jsou deklarovány v souboru *xsikor_LCD.h*.

### xsikor_LCD.c (.h)

Pro inicializaci LCD displeje a jeho dotykové části jsou vytvořeny následující funkce.

```C
uint8_t xsikor_LCD_INIT(void);
void xsikor_TS_INIT(void);
```

K těmto funkcím je snad jen třeba dodat, že při použití *BSP* knihoven je nutné po inicializaci displeje definovat výchozí hladinu displeje, a zvolit hladinu, která se má zobrazit. Bez těchto dvou řádků nebylo možné s displejem vůbec pracovat.
Displej disponuje dvěmi hladinami, které mohou být zobrazeny například přes sebe.

```C
ret = BSP_LCD_Init();
BSP_LCD_LayerDefaultInit(1, LCD_FRAME_BUFFER);
BSP_LCD_SelectLayer(1);
```

Dále je v souboru definována funkce pro tvorbu klávesnice. Kvůli snadnější práci je v kódu vytvořená struktura, která nese informaci o tlačítku. Obsahuje informaci o jeho velikosti, relativní pozici vzhledem k počátku klávesnice (levý horní roh) a popisek tlačítka.

```C
typedef struct struktura{
    uint16_t xpos_relative;
    uint16_t ypos_relative;
    uint16_t xsize;
    uint16_t ysize;
    uint8_t label[5];
}TKEY;
```

Funkce pro tvorbu klávesnice má jako vstupní parametr ukazatel na pole právě těchto struktur a dále rozměry klávesnice. Další informací, kterou funkce potřebuje, je počet řádků a počet sloupců tlačítek klávesnice. Obě tyto hodnoty jsou deklarovány jako konstanty v .h souboru.
Podle zmíněných informací vytvoří funkce klávesnici.

```C
void xsikor_make_keyboard(TKEY key[NUM_KEY_ROW][NUM_KEY_COL], uint16_t kb_xsize, uint16_t kb_ysize)
{
    uint8_t lab='1';

    for(uint8_t i=0; i<NUM_KEY_ROW;i++)
    {
        for(uint8_t y=0; y<NUM_KEY_COL;y++)
        {
            key[i][y].xsize = kb_xsize/NUM_KEY_COL;
            key[i][y].ysize = kb_ysize/NUM_KEY_ROW;
            key[i][y].xpos_relative = y*key[i][y].xsize;
            key[i][y].ypos_relative = i*key[i][y].ysize;
            key[i][y].label[0] = lab ;
            key[i][y].label[1] = '\0' ;

            if(lab == '1'+ 9)
            {
                key[i][y].label[0] = 'G';
                key[i][y].label[1] = 'E';
                key[i][y].label[2] = 'N';
                key[i][y].label[3] = '\0';
            }
            if(lab == '1' + 10)
            {
                key[i][y].label[0] = '0';
            }
            if(lab == '1' + 11)
            {
                key[i][y].label[0] = 'C';
                key[i][y].label[1] = 'L';
                key[i][y].label[2] = 'R';
                key[i][y].label[3] = '\0';
            }
            lab++;
        }
    }
    return;
}
```

Další důležitou částí kódu je funkce pro vykreslení klávesnice na displej. 

```C
void xsikor_print_keyboard(uint16_t xpos, uint16_t ypos, TKEY key[NUM_KEY_ROW][NUM_KEY_COL]);
```

Vstupními parametry této funkce jsou souřadnice levého horního rohu klávesnice a pole struktur kláves. Samotné vykreslení je realizováno funkcemi knihovny *BSP*.
Níže je uveden příklad (část funkce).

```C
BSP_LCD_SetTextColor(LCD_COLOR_GRAY);
BSP_LCD_FillRect(xpos+key[i][y].xpos_relative, ypos+key[i][y].ypos_relative, key[i][y].xsize, key[i][y].ysize);
BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
BSP_LCD_DrawRect(xpos+key[i][y].xpos_relative, ypos+key[i][y].ypos_relative, key[i][y].xsize, key[i][y].ysize);
BSP_LCD_SetFont(&Font20);
BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
BSP_LCD_SetBackColor(LCD_COLOR_GRAY);
BSP_LCD_DisplayStringAt((xpos+key[i][y].xpos_relative+(key[i][y].xsize/2))-(Font20.Width/2),(ypos+key[i][y].ypos_relative+(key[i][y].ysize/2))-(Font20.Height/2),key[i][y].label, LEFT_MODE );
```

V případě, že dojde k dotyku displeje, *BSP* funkce vrátí souřadnice místa stisku displeje. Aby bylo možné tuto informaci využít je vytvořená funkce, která vrátí index stisknutého tlačítka ve dvojrozměrném poli struktur. 
Pokud je dotyk na jiné části displeje než je klávesnice, funkce vrací hodnotu -1 v obou souřadnicích.

```C
void xsikor_key_index(int16_t *index_row, int16_t *index_column,uint16_t TS_xpos, uint16_t TS_ypos, TKEY keyboard[NUM_KEY_ROW][NUM_KEY_COL]);
```

Poslední funkci v souboru xsikor_LCD.c (.h) je funkce pro výpis časů výpočtů vzorků jednotlivými metodami.
```C
void xsikor_vypis_casy(int32_t cas[3]);
```

### main.c

Hlavní soubor obsahuje opět inicializace periferií. Hlavní části je nekonečná smyčka, která proběhne každých 250 ticků. Při každém oběhu se kontroluje, zda došlo v průběhu tohoto času k dotyku displeje. 
Pokud ano, je zahájeno "zpracování" doteku. Jsou určeny souřadnice místa dotyku, následně se určí zda byla zmáčknutá nějaká klávesnice a pokud ano, je příslušná klávesa zpracována třemi možnými způsoby.

Pokud je zmáčknuté jedno z čísel, je toto číslo uloženo do řetězce *frequency* obsahujícího informaci o zadávaném kmitočtu. Zároveň je z řetězce vytvořená číselná hodnota kmitočtu (*frequency_integer*). Součástí tohoto zpracování je i ošetření, aby první zadané číslo nemohlo být 0 a aby délka čísla nemohla 
být více než 5 pozic z důvodu omezeného místa na displeji.

```C
if (keyboard[index_row][index_column].label[0]>='0' && keyboard[index_row][index_column].label[0] <= '9')
{
   strcat(frequency,keyboard[index_row][index_column].label);
   if (frequency[0] == '0')
   {
	   frequency[0] = '\0';
   }
   else{
	   sscanf(frequency,"%d",&frequency_integer);
	   if (frequency_integer > 99999)
	   {
		   frequency[5] = '\0';
	   }

   }
}
```
Když je použito funkční tlačítko *CLR*, je řetězec *frequency* i hodnota *frequency_integer* resetována. Poslední možností je tlačítko *GEN*. Po jeho stisku je zkontrolováno, zda zadaná frekvence nepřesahuje povolené maximum,
pokud ano, hodnota se saturuje nejvyšší povolenou hodnotou definovanou jako konstanta v programu. Dále se vytvoří zpráva obsahující hodnotu nové frekvence s přesně daným formátem a odešle se neblokující funkcí *HAL_UART_Transmit_IT()*
po rozhraní UART do generátoru. 

Po vykonání jedné z uvedených funkcí se aktualizuje výpis na displeji.

```C
else if(!strcmp(keyboard[index_row][index_column].label, "CLR"))
{
	  frequency[0] = '\0';
	  frequency_integer=0;
	  BSP_LCD_ClearStringLine(1);
}
else if(!strcmp(keyboard[index_row][index_column].label, "GEN"))
{
	  if (frequency_integer > MAX_GENERATED_FREQ)
	  {
		  frequency_integer = MAX_GENERATED_FREQ;
		  sprintf(frequency, "%d",MAX_GENERATED_FREQ);

	  }
	  sprintf(uart_frequency, "%05d#",frequency_integer);
	  HAL_UART_Transmit_IT(&huart1, uart_frequency, 6);
}

sprintf(str_freq_out, "Fgen[Hz]: %s", frequency);
BSP_LCD_DisplayStringAtLine(1, str_freq_out);
```

# Závěr

## Naměřené časy

Porovnání různých způsobů přineslo zajímavé výsledky. Jako bezkonkurenčně nejrychlejší se ukázala být knihovna *arm_math.h* a její funkce

```C
void arm_sin_cos_f32 (float32_t theta, float32_t *pSinVal, float32_t *pCosVal);
```
Tato funkce počítá hodnoty goniometrických funkcí za použití tabulky hodnot a lineární interpolace. Funkce byla schopná vypočítat 10 000 vzorků za pouhých 17 milisekund. To odpovídá hodnotě 1,7 mikrosekundy na vzorek.
Druhý v pořadí byl algoritmus CORDIC v plovoucí řádové čárce, který dokázal vypočítat jeden vzorek za 7,9 mikrosekundy při 16 iteracích výpočtu.
Nejpomalejší byla matematická knihovna *math.h*. Funkce sinf() a cosf() potřebovaly pro výpočet jednoho vzorku dohromady 35 mikrosekund.

Dalo by se předpokládat, že algoritmus CORDIC v pevné řádové čárce by byl ještě o něco rychlejší než v plovoucí, nicméně se jej nepodařilo zprovoznit a tak to nelze tvrdit s jistotou.
## Zhodnocení projektu

V projektu se podařilo úspěšně realizovat generátor harmonického signálu pro sluchátkový výstup vývojové desky STM32F4Discovery. Generátor je snadno ovládatelný řídícím panelem na vývojové desce STM32F429I. 
Generátor umožňuje tvorbu signálu s libovolnou celočíselnou frekvencí od 1 do 20 000 Hz. Ovládací panel by měl být dostatečně ošetřen, aby nebylo možné zadat číslo, které by generátor neuměl zpracovat. Podle zadání se rovněž podařilo
porovnat různé způsoby výpočtu goniometrických funkcí. Při realizaci vznikl problém s výpočtem algoritmu CORDIC v pevné řádové čárce. Jak již bylo zmíněno, vznikla chyba pravděpodobně někde při bitových posuvech čísel ve formátu Q0.15.
Nicméně vzhledem k tomu, že procesor má zabudovanou jednotku pro operace v plovoucí řádové čárce lze předpokládat, že by výpočet v pevné čárce nebyl nijak extrémně rychlejší. 
Obecně je vidět, že ať už kvůli malé paměťové náročnosti, své přesnosti či možnosti generovat sinus i cosinus pro kvadraturní modulace, algoritmus může najít využití v praxi. O tom svědčí i fakt, že je tento algoritmus implementován v FPGA obvodech jako předpřipravený IP blok.
## Video ukázka
Bohužel vzhledem k omezeným možnostem je v ukázce zvuk snímán mikrofónem v počítači a nasledně je zobrazeno spektrum snímaného zvuku pomocí programu *Friture*.

[Ukázka](https://www.youtube.com/watch?v=rbe6vNscmYo)

