/*
 * cordic_fixed.h
 *
 *  Created on: 21. 11. 2019
 *      Author: Student
 */

#ifndef CORDIC_FIXED_H_
#define CORDIC_FIXED_H_

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include "arm_math.h"


#define KOREKCE_FIXED (1<<8) //hodnota, po jak� dob� se ma prov�st korekce
#define Niter_FIXED (16) //kvuli ulozenym zapornym hodnotam v KorekceAmplitudy je nutne aby byl pocet iteraci sudy a nema smysl aby jeho hodnota byla vetsi nez 30 vzhledem k posunu hodnot o (>> i)


#define	FLOAT2FIXED(x)	((x) >= 0 ? ((int16_t)((x)*32768+0.5)) : ((int16_t)((x)*32768-0.5)))
#define	FLOAT2FIXED3_28(x)	((x) >= 0 ? ((int32_t)((x)*268435456+0.5)) : ((int32_t)((x)*268435456-0.5)))
/*
Multiply the float by 2^(number of fractional bits for the type), eg. 2^8 for 24.8
Round the result (just add 0.5) if necessary, and floor it (or cast to an integer type) leaving an integer value.
Assign this value into the fixed-point type.
*/




#define JEDNA_PI (3.141592653589793)
#define DVA_PI   (6.283185307179586)
#define PI_PUL	 (1.570796326794897)
#define TRI_PI_PUL (4.712388980384690)

typedef struct struktura_fixed{
    int32_t redukovany_fazovy_inkrement;
    uint8_t kvadrant;
}TDPHI_FIXED;



void vypocet_korekcni_uhel_fixed(void);
void vypocet_korekce_amplitudy_fixed(void);
void vypocet_TDPHI_fixed(TDPHI_FIXED* dphi_fixed, int32_t fazovy_inkrement_fixed);
void cordic_rotacni_PK_fixed(int16_t *x, int16_t *y, int32_t fazovy_inkrement_fixed);
void cordic_rotacni_fixed(int16_t *x, int16_t *y, TDPHI_FIXED* dphi_fixed);


#endif /* CORDIC_FIXED_H_ */
