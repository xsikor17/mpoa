/*
 * cordic.h
 *
 *  Created on: 14. 11. 2019
 *      Author: Student
 */

#ifndef CORDIC_H_
#define CORDIC_H_

#include <math.h>
#include <stdint.h>
#include <stdio.h>

#define	FLOAT2FIXED(x)	((x) >= 0 ? ((int16_t)((x)*32768+0.5)) : ((int16_t)((x)*32768-0.5)))

/*
Multiply the float by 2^(number of fractional bits for the type), eg. 2^8 for 24.8
Round the result (just add 0.5) if necessary, and floor it (or cast to an integer type) leaving an integer value.
Assign this value into the fixed-point type.
*/

#define Niter (16) //pocet iteraci vypoctu

#define JEDNA_PI (3.141592653589793)
#define DVA_PI   (6.283185307179586)
#define PI_PUL   (1.570796326794897)
#define TRI_PI_PUL (4.712388980384690)

typedef struct struktura{
    float redukovany_fazovy_inkrement;
    uint8_t kvadrant;
}TDPHI;



void vypocet_korekcni_uhel(void);	//funkce kter� vypocita hodnoty atan(2^-i) pro i=0 a� Niter
void vypocet_korekce_amplitudy(void); //funkce kter� vypocita hodnoty cos(atan(2^-i)) pro i=0 a� Niter (korekcni hodnoty)
void vypocet_mocniny(void); //vzhledem k faktu ze float neumoznuje bitovy posun, ulozime si hodnoty 2^-i pro i=0 a� Niter
void vypocet_TDPHI(TDPHI* dphi, float fazovy_inkrement); //funkce, ktera redukuje fazovy inkrement v pripade ze je vetsi jak 90 stupnu a urci ve kterem kvadrantu by nova hodnota byla, to se nasledne vyuzije ke korekci vzorku
															//funkce resi problematiku maximalni rotace 99 stupnu viz teorie, vstupni parametr fazovy_inkrement muze nabyvat hodnot maximalne 360 stupnu, vzhledem k vzorkovacimu
															//teoremu vsak nebude nutne vice jak 180 stupnu

void cordic_rotacni_PK(float *x, float *y, float fazovy_inkrement); //funkce ktera spocita rotaci pouze do uhlu 99.xx stupnu
void cordic_rotacni(float *x, float *y, TDPHI* dphi); //funkce ktera vyuziva redukovany fazovy inkrement a je schopna vypocitat rotaci az o 360 stupnu

#endif /* CORDIC_H_ */
