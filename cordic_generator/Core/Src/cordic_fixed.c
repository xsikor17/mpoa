/*
 * cordic_fixed.c
 *
 *  Created on: 21. 11. 2019
 *      Author: Student
 */
#include "cordic_fixed.h"

//#include "hellocfg.h"

/*promenne vyuzite pouze v tomto skriptu, proto static (ostatni soubory nemaji pristup)*/
static int32_t KorekcniUhel_fixed[Niter_FIXED]; //pole s ulozenymi hodnotami dilcich uhlu fi, v prubehu vypoctu se nemeni
static int16_t KorekceAmplitudy_fixed[Niter_FIXED]; //pole s hodnotami korekce amplitudy vzorku
static float fazova_korekce_x_fp=0;
static float korekce_x_fp,korekce_y_fp;
/**/

/*GLOBALNI PROMENNE*/
extern int16_t lastx_fp,lasty_fp;
extern TDPHI_FIXED dphi_fp;
extern int32_t fazovy_inkrement_fp;
extern float fazovy_inkrement;
/**/




void vypocet_korekcni_uhel_fixed(void)
{
	uint16_t i;
	float pomocna;
	for (i=0 ; i<Niter_FIXED ; i++)
	{
		pomocna = atan(pow(2,(-i)));
	    KorekcniUhel_fixed[i] = FLOAT2FIXED3_28(pomocna) ;
	}

	return;
}

void vypocet_korekce_amplitudy_fixed(void)
{
    uint16_t i;
    float pomocna;
	for (i=0 ; i<Niter_FIXED ; i++)
	{
	  pomocna = cos(atan(pow(2,-i)));
      KorekceAmplitudy_fixed[i] = FLOAT2FIXED(-pomocna);
	 }
	 return;
}


void vypocet_TDPHI_fixed(TDPHI_FIXED* dphi_fixed, int32_t fazovy_inkrement_fixed)
{
    static int32_t pi_pul = FLOAT2FIXED3_28(PI_PUL);
    static int32_t jedna_pi = FLOAT2FIXED3_28(JEDNA_PI);
    static int32_t tri_pi_pul = FLOAT2FIXED3_28(TRI_PI_PUL);
    static int32_t dva_pi = FLOAT2FIXED3_28(DVA_PI);

    dphi_fixed->redukovany_fazovy_inkrement = fazovy_inkrement_fixed;

    if ((dphi_fixed->redukovany_fazovy_inkrement <= pi_pul) && (dphi_fixed->redukovany_fazovy_inkrement >= 0)){
		dphi_fixed->redukovany_fazovy_inkrement = dphi_fixed->redukovany_fazovy_inkrement;
		dphi_fixed->kvadrant=1;
	}else if ((dphi_fixed->redukovany_fazovy_inkrement > pi_pul) && (dphi_fixed->redukovany_fazovy_inkrement <= jedna_pi)){
		dphi_fixed->redukovany_fazovy_inkrement = __QSUB(dphi_fixed->redukovany_fazovy_inkrement,pi_pul);
		dphi_fixed->kvadrant=2;
	}else if ((dphi_fixed->redukovany_fazovy_inkrement > jedna_pi) && (dphi_fixed->redukovany_fazovy_inkrement<=(tri_pi_pul))){
		dphi_fixed->redukovany_fazovy_inkrement = __QSUB(dphi_fixed->redukovany_fazovy_inkrement,jedna_pi);
		dphi_fixed->kvadrant=3;
	}else if ((dphi_fixed->redukovany_fazovy_inkrement > (tri_pi_pul)) && (dphi_fixed->redukovany_fazovy_inkrement<= (dva_pi))){
		dphi_fixed->redukovany_fazovy_inkrement = __QSUB(dphi_fixed->redukovany_fazovy_inkrement,tri_pi_pul);
		dphi_fixed->kvadrant=4;
	}else{
		dphi_fixed->redukovany_fazovy_inkrement = 0;
		dphi_fixed->kvadrant=1;
	}
    return;
}

void cordic_rotacni_PK_fixed(int16_t *x, int16_t *y, int32_t fazovy_inkrement_fixed)
{
	static int16_t xtemp,ytemp,xold,yold;
	static int32_t z;
	static uint16_t i,posun;


	xtemp=*x;
	ytemp=*y;
	z=fazovy_inkrement_fixed;

	for (i=0 ; i<Niter_FIXED ; i++)
		{
			posun=i;
			if (i>=34)posun=34;
			xold = xtemp;
			yold = ytemp;

			if (z < 0)
			{
				xtemp =((int32_t)__QADD((int32_t)__SMUAD(KorekceAmplitudy_fixed[i],xold),((int32_t)__SMUAD(yold,KorekceAmplitudy_fixed[i]))>>posun))>>16;
				ytemp =((int32_t)__QSUB((int32_t)__SMUAD(KorekceAmplitudy_fixed[i],yold),((int32_t)__SMUAD(xold,KorekceAmplitudy_fixed[i]))>>posun))>>16;
				z = ((int32_t)__QADD(z,KorekcniUhel_fixed[i]));
			}else{
				xtemp =((int32_t)__QSUB((int32_t)__SMUAD(KorekceAmplitudy_fixed[i],xold),((int32_t)__SMUAD(yold,KorekceAmplitudy_fixed[i]))>>posun))>>16;
				ytemp =((int32_t)__QADD((int32_t)__SMUAD(KorekceAmplitudy_fixed[i],yold),((int32_t)__SMUAD(xold,KorekceAmplitudy_fixed[i]))>>posun))>>16;
				z =((int32_t)__QSUB(z,KorekcniUhel_fixed[i]));
			}

		}
    *x = xtemp;
	*y = ytemp;

	return;

}

void cordic_rotacni_fixed(int16_t *x, int16_t *y, TDPHI_FIXED* dphi_fixed)
{
	static int16_t xnew,ynew;
	static uint32_t i=1;
	xnew = *x;
	ynew = *y;

	cordic_rotacni_PK_fixed(&xnew,&ynew,dphi_fixed->redukovany_fazovy_inkrement);

    if (dphi_fixed->kvadrant == 1){
	    *x = xnew;
	    *y = ynew;
	}else if (dphi_fixed->kvadrant == 2){
	    *x = (__SMUAD(-32768,ynew))>>16;
	    *y = xnew;
	}else if (dphi_fixed->kvadrant == 3){
		*x = (__SMUAD(-32768,xnew))>>16;
		*y = (__SMUAD(-32768,ynew))>>16;
	}else if (dphi_fixed->kvadrant == 4){
		*x = ynew;
		*y =(__SMUAD(-32768,xnew))>>16;
	}
	#if 0		//aktivace korekce
			if(((i & (KOREKCE_FIXED-1)) == 0))
			{
				korekce_x_fp = cos((((KOREKCE_FIXED))*fazovy_inkrement)+fazova_korekce_x_fp);
				korekce_y_fp = sin((((KOREKCE_FIXED))*fazovy_inkrement)+fazova_korekce_x_fp);


				fazova_korekce_x_fp = acos(korekce_x_fp);

				if(korekce_y_fp<0)
					fazova_korekce_x_fp = DVA_PI - fazova_korekce_x_fp;

				if (korekce_x_fp >0.9999694824)
					korekce_x_fp = 0.9999694824;
				if (korekce_y_fp > 0.9999694824)
					korekce_y_fp = 0.9999694824;
				lastx_fp=FLOAT2FIXED(korekce_x_fp);
				lasty_fp=FLOAT2FIXED(korekce_y_fp);
			}
	#endif
	i++;

	return;
}
