/*
 * cordic.c
 *
 *  Created on: 14. 11. 2019
 *      Author: Student
 */

#include "cordic.h"

/*Globalni promenne pro aktualni skript*/
static float KorekcniUhel[Niter];
static float KorekceAmplitudy[Niter];
static float mocnina[Niter];
/**/

/*GLOBALNI PROMENNE PRO PROJEKT*/
extern float lastx,lasty;
extern TDPHI dphi;
extern float fazovy_inkrement;
/**/

void vypocet_korekcni_uhel(void)
{
	uint16_t i;
	for (i=0 ; i<Niter ; i++)
	{
	    KorekcniUhel[i] = (float)atan(pow(2,(-i)));
	}

	return;
}

void vypocet_korekce_amplitudy(void)
{
    uint16_t i;
	for (i=0 ; i<Niter ; i++)
	{
      KorekceAmplitudy[i] = (float)cos(atan(pow(2,-i)));
	}
	return;
}

void vypocet_mocniny(void)
{
	uint16_t i;
	for (i=0 ; i<Niter ; i++)
	{
	   mocnina[i] = (float) pow(2,(-i));
	}

    return;
}

void vypocet_TDPHI(TDPHI* dphi, float fazovy_inkrement)
{
    dphi->redukovany_fazovy_inkrement = fazovy_inkrement;

    if ((dphi->redukovany_fazovy_inkrement <= PI_PUL) && (dphi->redukovany_fazovy_inkrement >= 0)){
		dphi->redukovany_fazovy_inkrement = dphi->redukovany_fazovy_inkrement;
		dphi->kvadrant=1;
	}else if ((dphi->redukovany_fazovy_inkrement > PI_PUL) && (dphi->redukovany_fazovy_inkrement <= JEDNA_PI)){
		dphi->redukovany_fazovy_inkrement = (dphi->redukovany_fazovy_inkrement)-(PI_PUL);
		dphi->kvadrant=2;
	}else if ((dphi->redukovany_fazovy_inkrement > JEDNA_PI) && (dphi->redukovany_fazovy_inkrement<=(TRI_PI_PUL))){
		dphi->redukovany_fazovy_inkrement = dphi->redukovany_fazovy_inkrement-(JEDNA_PI);
		dphi->kvadrant=3;
	}else if ((dphi->redukovany_fazovy_inkrement > (TRI_PI_PUL)) && (dphi->redukovany_fazovy_inkrement<= (DVA_PI))){
		dphi->redukovany_fazovy_inkrement = dphi->redukovany_fazovy_inkrement-(TRI_PI_PUL);
		dphi->kvadrant=4;
	}else{
		dphi->redukovany_fazovy_inkrement = 0;
		dphi->kvadrant=1;
	}
    return;
}

void cordic_rotacni_PK(float *x, float *y, float fazovy_inkrement)
{
	static float xtemp,ytemp,z,xold,yold;
	static uint16_t i;


	xtemp=*x;
	ytemp=*y;
	z=fazovy_inkrement;

	for (i=0 ; i<Niter ; i++)
	{
		xold = xtemp;
		yold = ytemp;

		if (z < 0)
		{

			xtemp = ((KorekceAmplitudy[i]*(xold)) + (KorekceAmplitudy[i]*yold * mocnina[i]));
			ytemp = ((KorekceAmplitudy[i]*(yold)) - (KorekceAmplitudy[i]*xold * mocnina[i]));
			z = z + KorekcniUhel[i];
		}else{
			xtemp = ((KorekceAmplitudy[i]*(xold)) -(KorekceAmplitudy[i]*yold * mocnina[i]));
			ytemp = ((KorekceAmplitudy[i]*(yold)) +(KorekceAmplitudy[i]*xold * mocnina[i]));
			z = z - KorekcniUhel[i];
		}

	}
    *x = xtemp;
	*y = ytemp;

	return;

}

void cordic_rotacni(float *x, float *y, TDPHI* dphi)
{
	static float xnew,ynew;

	xnew = *x;
	ynew = *y;

	cordic_rotacni_PK(&xnew,&ynew,dphi->redukovany_fazovy_inkrement);

    if (dphi->kvadrant == 1){
	    *x = xnew;
	    *y = ynew;
	}else if (dphi->kvadrant == 2){
	    *x = -ynew;
	    *y = xnew;
	}else if (dphi->kvadrant == 3){
		*x = -xnew;
		*y = -ynew;
	}else if (dphi->kvadrant == 4){
		*x = ynew;
		*y = -xnew;
	}
	return;
}


